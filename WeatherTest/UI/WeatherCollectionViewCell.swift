//
//  WeatherCollectionViewCell.swift
//  WeatherTest
//
//  Created by Jon on 11/11/2018.
//  Copyright © 2018 develosphere. All rights reserved.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    func setWeather(_ weather: Weather) {
        tempLabel.text = String(format: "Temp: %.0f°C", weather.temperature.temp)
        windLabel.text = String(format: "Wind: %.0f", weather.wind.speed)
        let date = Date(timeIntervalSince1970: weather.date)
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        let day = formatter.string(from: date)
        if let description = weather.description.first, let data = try? Data(contentsOf: description.iconUrl) {
            icon.image = UIImage(data: data)
            descriptionLabel.text = String(format: "%@ - %@", day, description.description)
        }
        
        containerView.layer.cornerRadius = 20.0
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        containerView.layer.shadowRadius = 5.0
        containerView.layer.shadowOpacity = 0.7
    }
}
