//
//  Requests.swift
//  WeatherTest
//
//  Created by Jon on 11/11/2018.
//  Copyright © 2018 develosphere. All rights reserved.
//

import Foundation
import Conn

struct GetWeather: RequestType {
    typealias ResponseType = WeatherData
    var city: String
    var data: RequestData {
        let path = String(format: "https://api.openweathermap.org/data/2.5/forecast?q=%@&appid=d6ad2ee5e4d6a2fada17519a8de0dac3&units=metric", city)
        return RequestData(path: path)
    }
}
