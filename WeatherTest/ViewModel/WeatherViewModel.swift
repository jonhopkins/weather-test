//
//  WeatherViewModel.swift
//  WeatherTest
//
//  Created by Jon on 11/11/2018.
//  Copyright © 2018 develosphere. All rights reserved.
//

import Foundation

class WeatherViewModel {
    private var model: WeatherModel!
    var weather: WeatherData?
    
    init(model: WeatherModel) {
        self.model = model
    }
    
    func getWeather(onSuccess: @escaping ([Weather]) -> Void,
                    onError: @escaping (Error) -> Void) {
        GetWeather(city: model.city).execute(onSuccess: { [unowned self] data in
            onSuccess(self.filterData(data.list))
        }) { (error) in
            self.weather = nil
            onError(error)
        }
    }
    
    private func filterData(_ list: [Weather]) -> [Weather] {
        var newList = [Weather]()
        let calendar = Calendar.current
        let today = calendar.component(.day, from: Date())
        for day in today...today + 5 {
            let list = list.filter { $0.dayNumber == day }
            if let weather = list.first {
                newList.append(weather)
            }
        }
        return newList
    }
}
