//
//  ViewController.swift
//  WeatherTest
//
//  Created by Jon on 11/11/2018.
//  Copyright © 2018 develosphere. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private var viewModel = WeatherViewModel(model: WeatherModel(city: "London,uk"))
    private var weatherList: [Weather]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getWeather(onSuccess: { [weak self] list in
            self?.weatherList = list
            self?.collectionView.reloadData()
        }) { [weak self] error in
            self?.weatherList = nil
            self?.collectionView.reloadData()
        }
    }
}

extension WeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let weatherList = weatherList else { return 0 }
        return weatherList.count > 5 ? 5 : weatherList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherCell", for: indexPath) as! WeatherCollectionViewCell
        guard let weatherList = weatherList else { return UICollectionViewCell() }
        let weather = weatherList[indexPath.row]
        cell.setWeather(weather)
        return cell
    }
    
    
}

