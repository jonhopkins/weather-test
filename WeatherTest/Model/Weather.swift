//
//  Weather.swift
//  WeatherTest
//
//  Created by Jon on 11/11/2018.
//  Copyright © 2018 develosphere. All rights reserved.
//

import Foundation

struct WeatherData: Codable {
    var list: [Weather]
}

struct Weather: Codable {
    let temperature: TemperatureData
    let description: [WeatherDescription]
    let wind: WindData
    let rain: RainData
    let date: TimeInterval
    var dayNumber: Int {
        let dateVar = Date(timeIntervalSince1970: date)
        let calendar = Calendar.current
        return calendar.component(.day, from: dateVar)
    }
    
    enum CodingKeys: String, CodingKey {
        case temperature = "main"
        case description = "weather"
        case wind = "wind"
        case rain = "rain"
        case date = "dt"
    }
}

struct TemperatureData: Codable {
    let temp: Double
    let temp_min: Double
    let temp_max: Double
}

struct WeatherDescription: Codable {
    let description: String
    let icon: String
    var iconUrl: URL {
        return URL(string: String(format:"https://openweathermap.org/img/w/%@.png", icon))!
    }
}

struct WindData: Codable {
    let speed: Double
}

struct RainData: Codable {
    let amount: Double?
    
    enum CodingKeys: String, CodingKey {
        case amount = "3h"
    }
}
